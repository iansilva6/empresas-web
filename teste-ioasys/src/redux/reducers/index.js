import { combineReducers } from 'redux';
import login from './loginReducer';
import companies from './companiesReducer';

const appReducer = combineReducers({
    login,
    companies
});

const rootReducer = (state, action) => {
    return appReducer(state, action);
}

export default rootReducer;