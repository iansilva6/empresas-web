import initialState from '../initialState';
import * as types from '../actions/actionTypes';

export default function loginReducer(state = initialState.login, action) {
    switch (action.type) {
        case types.LOGIN: {
            return {
                ...state,
                isLoading: true
            };
        }

        case types.LOGIN_SUCCESS: {
            localStorage.setItem("access-token", action.headers["access-token"]);
            localStorage.setItem("uid", action.headers["uid"]);
            localStorage.setItem("client", action.headers["client"]);
            return {
                ...state,
                isLoading: false,
                loginSuccess: true
            };
        }

        case types.LOGIN_ERROR: {
            return {
                ...state,
                isLoading: false,
                loginSuccess: false
            };
        }

        case types.RESET_LOGIN: {
            return {
                ...state,
                [action.prop]: undefined
            };
        }

        default:
            return state;
    }
}