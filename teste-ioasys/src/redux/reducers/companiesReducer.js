import initialState from '../initialState';
import * as types from '../actions/actionTypes';

export default function companiesReducer(state = initialState.companies, action) {
    switch (action.type) {
        case types.GET_COMPANIES_SUCCESS: {
            return {
                ...state,
                companiesList: action.data
            };
        }

        case types.GET_COMPANIE_INFO_SUCCESS: {
            return {
                ...state,
                companieInfo: action.data
            };
        }

        case types.RESET_COMPANIES: {
            return {
                ...state,
                [action.prop]: undefined
            };
        }

        default:
            return state;
    }
}