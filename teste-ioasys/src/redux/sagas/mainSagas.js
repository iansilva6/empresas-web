/* eslint-disable no-unused-vars */
import { takeEvery } from 'redux-saga/effects';
import { put } from 'redux-saga/effects';
import api from '../../utils/api';
import * as types from '../actions/actionTypes';

function* sagaPost({type, url, args}) {
    try {
        const response = yield api.HttpPost(url, args);
        yield put({ type: `${type}_SUCCESS`, data: response.data, headers: response.headers });
        
    } catch (error) {
        yield put({ type: `${type}_ERROR`, error });
    }
}

function* sagaGet({type, url, args}) {
    try {
        let response;

        // Add access token, uid and cient in headers
        api.setToken();

        if(args) {
            response = yield api.HttpGet(`${url}/${args}`);
        } else {
            response = yield api.HttpGet(url);
        }

        if(response.data) {
            yield put({ type: `${type}_SUCCESS`, data: response.data });
        } else {
            yield put({ type: `${type}_ERROR`, error: response.data });
        }
    } catch (error) {
        const originalRequest = error.config;

        if (error.response.status === 401 && !originalRequest._retry) {
            localStorage.removeItem('access-token');
            localStorage.removeItem('uid');
            localStorage.removeItem('client');
        }
        
        yield put({ type: `${type}_ERROR`, error });
    }
}
  
function* sagaDelete({type, url, args}) {
    try {
        const response = yield api.HttpDelete(`${url}/${args}`);

        if(response.data.success) {
            yield put({ type: `${type}_SUCCESS`, data: response.data });
        } else {
            yield put({ type: `${type}_ERROR`, error: response.data });
        }
    } catch (error) {
        yield put({ type: `${type}_ERROR`, error });
    }
}
  
function* sagaPatch({type, url, args}) {
    try {
        const response = yield api.HttpPatch(url, args);

        if(response.data.success) {
            yield put({ type: `${type}_SUCCESS`, data: response.data });
        } else {
            yield put({ type: `${type}_ERROR`, error: response.data });
        }
    } catch (error) {
        yield put({ type: `${type}_ERROR`, error });
    }
}
  
export function* watchEntity() {
    //LOGIN
    yield takeEvery(types.LOGIN, sagaPost);
    //COMPANIES
    yield takeEvery(types.GET_COMPANIES, sagaGet);
    yield takeEvery(types.GET_COMPANIE_INFO, sagaGet);
}