import * as types from './actionTypes';

const dispatch = (type, url, args) => {
    return { type, url, args }
}

export function login(data) {
    return dispatch(types.LOGIN, '/users/auth/sign_in', data);
}

export function resetLogin(prop){
    return {type: types.RESET_LOGIN, prop }
}