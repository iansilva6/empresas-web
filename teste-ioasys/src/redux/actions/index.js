import * as login from './loginActions';
import * as companies from './companiesActions';

export default {
    login,
    companies
}