import * as types from './actionTypes';

const dispatch = (type, url, args) => {
    return { type, url, args }
}

export function getCompanies(data) {
    return dispatch(types.GET_COMPANIES, `/enterprises?name=${data.name}`);
}

export function getCompanieInfo(data) {
    return dispatch(types.GET_COMPANIE_INFO, `/enterprises/${data.id}`);
}

export function resetCompanies(prop){
    return {type: types.RESET_COMPANIES, prop }
}