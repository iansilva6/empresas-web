import React from "react";
import { Route, Redirect } from "react-router-dom";

const PrivateRoute = ({ component: Component, ...rest }) => (
    <Route 
        {...rest} 
        render={(props) =>
            localStorage.getItem("access-token") 
            ? 
            (<Component {...props} />) 
            : 
            (<Redirect exact to="/" />)
        } 
    />
);

export const getRoutes = (name, routes) => {
    return routes.map((prop, key) => {

        if (prop.collapse) {
            return getRoutes(prop.layout, prop.views);
        }

        if (prop.layout === name) {
            if(prop.protected) {
                if (localStorage.getItem("access-token")) {
                    return (
                        <PrivateRoute 
                            path={prop.layout + prop.path}
                            component={prop.component}
                            key={key}
                        />
                    )
                } else {
                    return (<Redirect exact to="/" />)
                } 
            } else {
                return (
                    <Route
                        path={prop.layout + prop.path}
                        component={prop.component}
                        key={key}
                    />
                )
            }
        } else {
            return null;
        }
    });
};