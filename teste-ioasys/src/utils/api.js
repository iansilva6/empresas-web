import axios from 'axios';

class Axios {
    constructor() {
        this.baseURL = "https://empresas.ioasys.com.br/api/v1/";
        this.token = axios.create({
            baseURL: this.baseURL,
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        });

        this.HttpGet = this.HttpGet.bind(this);
        this.HttpPost = this.HttpPost.bind(this);
        this.HttpPatch = this.HttpPatch.bind(this);
        this.HttpDelete = this.HttpDelete.bind(this);
        this.setToken = this.setToken.bind(this);
    }

    HttpGet(url) {
        return this.token.get(url);
    }

    HttpPost(url, model) {
        return this.token.post(url, model);
    }

    HttpPatch(url, model) {
        return this.token.patch(url, model)
    }

    HttpDelete(url) {
        return this.token.delete(url);
    }

    /**
     * Sets the Token in the HTTP header for the auth user
     * @param {string} token - OAuth 2.0 / Bearer Token 
     */
    setToken()
    {
        this.token = axios.create({
            baseURL: this.baseURL,
            responseType: 'json',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'access-token': localStorage.getItem("access-token"),
                'uid': localStorage.getItem("uid"),
                'client': localStorage.getItem("client")
            }
        });
    }
}

const instance = new Axios();

export default instance;

