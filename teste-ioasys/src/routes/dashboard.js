// Views
import Home from "../views/Companies";
import CompanieInfo from "../views/CompanieInfo";

const layout = "/empresas";

const dashboardRoutes = [
    {
        path: "/:id",
        name: "Empresa",
        protected: true,
        component: CompanieInfo,
        layout
    },
    {
        path: "/",
        name: "Home",
        protected: true,
        component: Home,
        layout
    }
]

export default dashboardRoutes;