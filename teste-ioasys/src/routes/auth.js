//Views
import Login from "../views/Login";
import Dashboard from "../views/Dashboard";

const authRoutes = [
    { path: "/empresas", name: "Empresas", component: Dashboard, layout: "" },
    { path: "/", name: "Login", component: Login, layout: "" }
]

export default authRoutes;