import React from 'react';

// Styles
import { Button } from './styles';

function PrimaryButton(props) {
    return (
        <Button onClick={() => props.actionFunction()}>{props.label}</Button>
    );
}

export default PrimaryButton;