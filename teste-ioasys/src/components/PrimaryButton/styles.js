//Libs
import styled from 'styled-components';

export const Button = styled.button`
    margin: 10px 20px;
    padding: 0.9rem 7.919rem 0.9rem 7.956rem;
    border-radius: 3.6px;
    background-color: #57bbbc;
    border: none;
    cursor: pointer;
    font-family: Roboto;
    font-size: 1.125rem;
    font-weight: bold;
    font-stretch: normal;
    font-style: normal;
    line-height: normal;
    letter-spacing: normal;
    text-align: center;
    color: #fff;
    text-transform: uppercase;
    font-family: 'Roboto', sans-serif;
`;