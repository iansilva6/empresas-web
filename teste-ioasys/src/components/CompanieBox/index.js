import React from 'react';

// Libs
import { Image } from 'react-bootstrap';

// Styles
import { Content, Text } from './styles';


function CompanieBox(props) {
    return (
        <Content onClick={props.actionFunction}>
            <Image src={"https://empresas.ioasys.com.br/"+props.photo}></Image>
            <Text>
                <h2>{props.name}</h2>
                <h3>{props.companie_type_name}</h3>
                <h4>{props.city + " - " + props.country}</h4>
            </Text>
        </Content>
    );
}

export default CompanieBox;