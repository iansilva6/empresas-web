//Libs
import styled from 'styled-components';

export const Content = styled.div`
    background: #fff;
    padding: 40px;
    width: 100%;
    margin-bottom: 40px;
    display: flex;
    justify-content: flex-start;
    cursor: pointer;
    @media(max-width: 800px) {
        display: block;
    }
    img {
        margin-right: 40px;
        display: block;
        max-width: 100%;
    }
`;

export const Text = styled.div`
    padding: 20px 0px;
    h2 {
        color: #1a0e49;
        font-size: 1.875rem;
        font-weight: bold;
        font-stretch: normal;
        font-style: normal;
        line-height: normal;
        letter-spacing: normal;
        text-align: left;
    }
    h3 {
        color: #8d8c8c;
        font-size: 1.5rem;
        font-weight: normal;
        font-stretch: normal;
        font-style: normal;
        line-height: normal;
        letter-spacing: normal;
        text-align: left;
    }
    h4 {
        font-size: 1.125rem;
        font-weight: normal;
        font-stretch: normal;
        font-style: normal;
        line-height: normal;
        letter-spacing: normal;
        text-align: left;
        color: #8d8c8c;
    }
`;