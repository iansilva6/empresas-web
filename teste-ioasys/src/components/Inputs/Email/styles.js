//Libs
import styled from 'styled-components';

//Icons
import MailIcon from '../../../assets/icons/ic-email.svg';

export const Field = styled.div`
    position: relative;
    input {
        font-size: 1.125rem;
        font-weight: normal;
        font-stretch: normal;
        font-style: normal;
        line-height: normal;
        letter-spacing: -0.25px;
        text-align: left;
        border: none;
        background: transparent;
        border-bottom: 0.7px solid ${props => props.wrongField ? "#ff0f44" : "#383743"};
        width: 100%;
        font-family: 'Roboto', sans-serif;
        padding: 7px 35px;
        margin-bottom: 25px;
    }
    ::before {
        display: block;
        content: "";
        position: absolute;
        left: 0;
        width: 30px;
        height: 35px;
        background: url(${MailIcon})center no-repeat;
    }
    ::after {
        display: ${props => props.wrongField ? "block" : "none"};
        opacity: ${props => props.wrongField ? "1" : "0"};
        content: "!";
        position: absolute;
        right: 0;
        top: 0;
        width: 25px;
        height: 25px;
        border-radius: 50%;
        background: #ff0f44;
        display: flex;
        align-items: center;
        justify-content: center;
        color: #ffffff;
    }
`;