import React from 'react';

// Styles
import { Field } from './styles';

function Email(props) {
    return (
        <Field 
            wrongField={props.wrongField} 
            onChange={(e) => props.changeFunction('email', e)}
            onKeyDown={(e) => props.onKeyDownFunction(e)}
        >
            <input placeholder={"E-mail"}></input>
        </Field>
    );
}

export default Email;