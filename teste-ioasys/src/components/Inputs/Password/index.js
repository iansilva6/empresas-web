import React, { useState } from 'react';

// Libs
import { BsFillEyeFill, BsFillEyeSlashFill } from "react-icons/bs";


// Styles
import { Field, ShowIcon } from './styles';

function Password(props) {

    const [showPassword, setShowPassword] = useState(false);

    function showPasswordFunction() {
        setShowPassword(!showPassword);
    }

    return (
        <Field 
            wrongField={props.wrongField} 
            onChange={(e) => props.changeFunction('password', e)}
            onKeyDown={(e) => props.onKeyDownFunction(e)}
        >
            <input type={showPassword ? "text" : "password"} placeholder={"Senha"}></input>
            <ShowIcon onClick={() => showPasswordFunction()}>
                {
                    !props.wrongField
                    ?
                    showPassword
                        ?
                        <BsFillEyeSlashFill
                            size={22} 
                        />
                        :
                        <BsFillEyeFill
                            size={22}
                        />
                    :
                    ""
                }
            </ShowIcon>
        </Field>
    );
}

export default Password;