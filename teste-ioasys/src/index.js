import React from 'react';
import ReactDOM from 'react-dom';

// Routes Imports
import { createBrowserHistory } from "history";
import { Router, Switch } from "react-router-dom";
import authRoutes from "./routes/auth";
import { getRoutes } from "./utils/getRoutes";

// Redux
import configStore from './redux/configureStore';
import { Provider } from 'react-redux';

// Report Web Vitals
import reportWebVitals from './reportWebVitals';

// Global Css
import { GlobalStyle } from './assets/styles/global';

// FontFace
import 'typeface-roboto';

// Bootstrap
import 'bootstrap/dist/css/bootstrap.min.css';

const hist = createBrowserHistory();
const store = configStore();

ReactDOM.render(
    <React.StrictMode>
    {/* CSS Global */}
    <GlobalStyle />
        <Provider store={store}>
            <Router history={hist}>
                <Switch>
                    {
                        getRoutes("", authRoutes)
                    }
                </Switch>
            </Router>
        </Provider>
    </React.StrictMode>,
    document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
