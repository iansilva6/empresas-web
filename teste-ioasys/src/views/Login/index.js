/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect } from 'react';

// Libs
import { Image, Spinner } from 'react-bootstrap';

// Redux
import { connect } from 'react-redux';
import { login, resetLogin } from '../../redux/actions/loginActions';

// Components
import PrimaryButton from '../../components/PrimaryButton';
import InputMail from '../../components/Inputs/Email';
import InputPassword from '../../components/Inputs/Password';

// Styles
import { Wrapper, Form, Fields, WrongCredentials, Loading } from './styles';

// Images and Icons
import IoasysLogo from '../../assets/images/logo-home.png';

function Login(props) {

    // States
    const [wrongCredentials, setWrongCredentials] = useState(false);
    const [isLoading, setIsLoading] = useState(false);
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");

    // Clear credentials aways access this page
    useEffect(() => {
        localStorage.removeItem('access-token');
        localStorage.removeItem('uid');
        localStorage.removeItem('client');
    }, []);

    // Show loading while make request
    useEffect(() => {
        if (props.isLoading !== undefined) {
            setIsLoading(props.isLoading);
        }
    }, [props.isLoading]);

    // Show mesage when credentials are wrong
    useEffect(() => {
        if (props.loginSuccess !== undefined) {
            if(props.loginSuccess === true) {
                setWrongCredentials(false);
                redirectToDashboard();
            } else {
                setWrongCredentials(true);
            }
            props.resetLogin("loginSuccess");
        }
    }, [props.loginSuccess]);

    // Handle to change state when user typing in input
    function handleChange(prop, event) {
        if (prop === 'email') {
            setEmail(event.target.value);
        }
        if (prop === 'password') {
            setPassword(event.target.value);
        }
    }

    // Redirect to companies view
    function redirectToDashboard() {
        props.history.push(`/empresas`);
    }

    // Submit form when press enter
    function pressEnter(event) {
        if(event.keyCode === 13) {
            handleSubmit();
        }
    }

    function handleSubmit() {
        login();
    }

    // Request - Login
    function login() {
        props.login({
            email: email,
            password: password
        });
    }

    return (
        <Wrapper>
            {/* Show this spinner while loading request */}
            {
                isLoading
                ?
                <Loading>
                    <Spinner animation="border" />
                </Loading>
                :
                ""
            }
            <Form>
                {/* Ioasys Logo */}
                <Image src={IoasysLogo}></Image>
                <h2>BEM-VINDO AO<br></br>EMPRESAS</h2>
                <p>Lorem ipsum dolor sit amet, contetur<br></br>adipiscing elit. Nunc accumsan.</p>
                {/* Inputs - Email and Password */}
                <Fields>
                    <InputMail
                        wrongField={wrongCredentials}
                        value={email}
                        changeFunction={handleChange}
                        onKeyDownFunction={pressEnter}
                    />
                    <InputPassword
                        wrongField={wrongCredentials}
                        value={password}
                        changeFunction={handleChange}
                        onKeyDownFunction={pressEnter}
                    />
                </Fields>
                {/* Wrong Fields Message */}
                <WrongCredentials>
                    {
                        wrongCredentials
                        ?
                        "Credenciais informadas são inválidas, tente novamente."
                        :
                        ""
                    }
                </WrongCredentials>
                {/* Login Button */}
                <PrimaryButton 
                    label={"Enviar"}
                    actionFunction={login}
                />
            </Form>
        </Wrapper>
    );
}

const mapStateToProps = state => {
    return {
        loginSuccess: state.login.loginSuccess,
        isLoading: state.login.isLoading
    }
};
  
const mapDispatchToProps = dispatch => {
    return {
        login: (data) => dispatch(login(data)),
        resetLogin: (data) => dispatch(resetLogin(data)),
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);