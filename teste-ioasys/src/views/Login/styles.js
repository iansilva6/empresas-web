//Libs
import styled from 'styled-components';

export const Wrapper = styled.div`
    background: #ebe9d7;
    display: flex;
    align-items: center;
    justify-content: center;
    height: 100%;
`;

export const Form = styled.div`
    margin: 20px 0px;
    h2 {
        text-align: center;
        font-family: Roboto;
        font-size: 1.5rem;
        font-weight: bold;
        font-stretch: normal;
        font-style: normal;
        line-height: normal;
        letter-spacing: normal;
        color: #383743;
        margin-bottom: 20px;
    }
    img {
        margin: 0 auto;
        display: block;
        padding: 25px 0px;
    }
    p {
        font-family: Roboto;
        font-size: 1.125rem;
        font-weight: normal;
        font-stretch: normal;
        font-style: normal;
        line-height: 1.44;
        letter-spacing: -0.25px;
        text-align: center;
        color: #383743;
    }
`;

export const Fields = styled.div`
    width: 100%;
    margin: 20px 0px;
`;

export const Loading = styled.div`
    width: 100%;
    height: 100%;
    position: fixed;
    z-index: 100;
    -webkit-backdrop-filter: blur(1px);
    backdrop-filter: blur(1px);
    background-color: rgba(255, 255, 255, 0.6);
    display: flex;
    align-items: center;
    justify-content: center;
    .spinner-border {
        border-color: #57bbbc;
        border-right-color: transparent;
        width: 7rem;
        height: 7rem;
        border-width: .70em; 
    }
`;

export const WrongCredentials = styled.span`
    width: 100%;
    display: block;
    font-size: 0.76rem;
    font-weight: normal;
    font-stretch: normal;
    font-style: normal;
    line-height: 1.95;
    letter-spacing: -0.17px;
    text-align: center;
    color: #ff0f44;
    margin-bottom: 10px;
`;