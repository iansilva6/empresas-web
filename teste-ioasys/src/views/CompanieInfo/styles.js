//Libs
import styled from 'styled-components';

export const Nav = styled.div`
    padding: 2rem 1rem;
    background: #ee4c77;
    display: flex;
    justify-content: flex-start;
    h1 {
        font-size: 2.125rem;
        font-weight: normal;
        font-stretch: normal;
        font-style: normal;
        line-height: normal;
        letter-spacing: normal;
        color: #fff;
    };
`;

export const Back = styled.div`
    margin-right: 20px;
    cursor: pointer;
`;

export const Content = styled.div`
    background: #ebe9d7;
    padding: 40px 0px;
    min-height: 100%;
`;

export const Info = styled.div`
    background: #fff;
    padding: 40px;
    width: 100%;
    margin-bottom: 40px;
    display: block;
    img {
        display: block;
        margin: 0 auto;
        max-width: 100%;
    }
    p {
        margin: 3rem 0.438rem 0 0.563rem;
        font-size: 2.125rem;
        font-weight: normal;
        font-stretch: normal;
        font-style: normal;
        line-height: normal;
        letter-spacing: normal;
        color: #8d8c8c;
        text-align: justify;
    }
`;