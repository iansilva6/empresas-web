/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect } from 'react';

// Redux
import { connect } from 'react-redux';
import { getCompanieInfo, resetCompanies } from '../../redux/actions/companiesActions';

// Libs
import { Container, Row, Col, Image } from 'react-bootstrap';
import { BsArrowLeftShort } from "react-icons/bs";

// Styles
import { Nav, Back, Content, Info } from './styles';

function CompanieInfo(props) {

    const [companieInfoState, setCompanieInfoState] = useState({});

    // Effect to make request aways change input value
    useEffect(() => {
        getCompanieInfo(props.match.params.id);
    }, []);

    // Get companies list from request
    useEffect(() => {
        if (props.companieInfo !== undefined) {
            setCompanieInfoState(props.companieInfo.enterprise);
        }
        props.resetCompanies("companieInfo");
    }, [props.companieInfo]);

    // Request - Get Companies
    function getCompanieInfo(id) {
        props.getCompanieInfo({
            id: id
        });
    }

    // Redirect to companies view
    function redirectToHome() {
        props.history.push(`/empresas`);
    }

    return (
        <>
            <Nav>
                <Back onClick={() => redirectToHome()}>
                    <BsArrowLeftShort
                        color="white"
                        size={42}
                    />
                </Back>
                <h1>{companieInfoState?.enterprise_name ? companieInfoState?.enterprise_name : ""}</h1>
            </Nav>
            <Content>
                <Container>
                    <Row>
                        <Col sm={12}>
                            <Info>
                                <Image src={"https://empresas.ioasys.com.br/"+companieInfoState.photo}></Image>
                                <p>
                                    {companieInfoState?.description ? companieInfoState?.description : ""} 
                                </p>
                            </Info>
                        </Col>
                    </Row>
                </Container>
            </Content>
        </>
    );
}

const mapStateToProps = state => {
    return {
        companieInfo: state.companies.companieInfo,
    }
};
  
const mapDispatchToProps = dispatch => {
    return {
        getCompanieInfo: (data) => dispatch(getCompanieInfo(data)),
        resetCompanies: (data) => dispatch(resetCompanies(data)),
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(CompanieInfo);