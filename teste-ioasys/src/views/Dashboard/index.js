/* eslint-disable react-hooks/exhaustive-deps */
import React from 'react';

// Redux
import { connect } from 'react-redux';

// Routes
import dashboardRoutes from "../../routes/dashboard";
import { getRoutes } from "../../utils/getRoutes";
import { Switch } from "react-router-dom";

function Dashboard(props) {
    return (
        <Switch>{getRoutes('/empresas', dashboardRoutes)}</Switch>
    );
}

const mapStateToProps = state => {
    return {
    }
};

const mapDispatchToProps = dispatch => {
    return {
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);