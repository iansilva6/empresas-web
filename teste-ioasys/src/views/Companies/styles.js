//Libs
import styled from 'styled-components';

//Icons
import SearchIcon from '../../assets/icons/ic-search.svg';

export const Nav = styled.div`
    padding: 2rem 1rem;
    background: #ee4c77;
    .default-view {
        width: 100%;
        display: flex;
        justify-content: space-between;
    }
    .searching-view {

    }
`;

export const Search = styled.div`
    cursor: pointer;
`;

export const Field = styled.div`
    position: relative;
    input {
        font-size: 2.125rem;
        font-weight: normal;
        font-stretch: normal;
        font-style: normal;
        line-height: normal;
        letter-spacing: -0.25px;
        text-align: left;
        border: none;
        background: transparent;
        border-bottom: 0.7px solid #fff;
        width: 100%;
        font-family: 'Roboto', sans-serif;
        padding: 10px 70px;
        color: #fff;
        ::placeholder {
            color: #991237;
        }
    }
    ::before {
        display: block;
        content: "";
        position: absolute;
        left: 0;
        top: 0;
        width: 55px;
        height: 55px;
        background: url(${SearchIcon})center no-repeat;
    }
`;

export const Clear = styled.div`
    cursor: pointer;
    position: absolute;
    right: 0;
    top: 0;
`;

export const Content = styled.div`
    background: #ebe9d7;
    padding: 40px 0px;
    min-height: 100%;
    p {
        font-size: 2rem;
        font-weight: normal;
        font-stretch: normal;
        font-style: normal;
        line-height: 1.22;
        letter-spacing: -0.45px;
        text-align: center;
        color: #383743;
    }
    .not-found-data {
        font-size: 2.125rem;
        font-weight: normal;
        font-stretch: normal;
        font-style: normal;
        line-height: normal;
        letter-spacing: normal;
        text-align: center;
        color: #b5b4b4;
        width: 100%;
        padding: 10% 0%;
    }
`;