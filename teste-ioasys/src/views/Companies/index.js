/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect } from 'react';

// Redux
import { connect } from 'react-redux';
import { getCompanies, resetCompanies } from '../../redux/actions/companiesActions';

// Libs
import { Container, Row, Col, Image } from 'react-bootstrap';

// Components
import CompanieBox from '../../components/CompanieBox';

// Styles
import { Content, Nav, Search, Field, Clear } from './styles';

// Images and Icons
import NavLogo from '../../assets/images/logo-nav.png';
import SearchIcon from '../../assets/icons/ic-search.svg';
import CloseIcon from '../../assets/icons/ic-close.svg';

function Companies(props) {

    const [isSearching, setIsSearching] = useState(false);
    const [searchName, setSearchName] = useState("");
    const [companiesListState, setCompaniesListState] = useState({});

    // Effect to make request aways change input value
    useEffect(() => {
        getCompanies();
    }, [searchName]);

    // Get companies list from request
    useEffect(() => {
        if (props.companiesList !== undefined) {
            setCompaniesListState(props.companiesList.enterprises);
        }
        props.resetCompanies("companiesList");
    }, [props.companiesList]);

    // Handle to change state when user typing in input
    function handleChange(prop, event) {
        if (prop === 'searchName') {
            setSearchName(event.target.value);
            getCompanies();
        }
    }

    // Request - Get Companies
    function getCompanies() {
        props.getCompanies({
            name: searchName
        });
    }

    // Redirect to companie info
    function redirectToInfo(id) {
        console.log("passou")
        props.history.push(`/empresas/`+id);
    }

    return (
        <>
            <Nav>
                {
                    isSearching
                    ?
                    <div className={"searching-view"}>
                        <Field onChange={(e) => handleChange('searchName', e)}>
                            <input placeholder={"Pesquisar"}></input>
                            <Clear>
                                <Image src={CloseIcon} onClick={() => setIsSearching(false)}></Image>
                            </Clear>
                        </Field>
                    </div>
                    :
                    <div className={"default-view"}>
                        <div></div>
                        <Image src={NavLogo}></Image>
                        <Search>
                            <Image src={SearchIcon} onClick={() => setIsSearching(true)}></Image>
                        </Search>
                    </div>
                } 
            </Nav>
            {
                isSearching
                ?
                <Content>
                    <Container>
                        <Row>
                            {
                                companiesListState.length >= 1
                                ?
                                companiesListState.map(function(item,i) {
                                    return (
                                        <Col sm={12}>
                                            <CompanieBox
                                                photo={item.photo}
                                                name={item.enterprise_name}
                                                companie_type_name={item.enterprise_type.enterprise_type_name}
                                                city={item.city}
                                                country={item.country}
                                                actionFunction={() => redirectToInfo(item.id)}
                                            />
                                        </Col>
                                    )
                                })
                                :
                                <p className={"not-found-data"}>Nenhuma empresa foi encontrada para a busca realizada.</p>
                            }
                        </Row>
                    </Container>
                </Content>
                :
                <Content className={"d-flex justify-content-center align-items-center"}>
                    <p>Clique na busca para iniciar.</p>
                </Content>
            }  
        </>
    );
}

const mapStateToProps = state => {
    return {
        companiesList: state.companies.companiesList,
    }
};
  
const mapDispatchToProps = dispatch => {
    return {
        getCompanies: (data) => dispatch(getCompanies(data)),
        resetCompanies: (data) => dispatch(resetCompanies(data)),
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Companies);