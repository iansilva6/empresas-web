# ioasys Empresas - Teste Prático

---

Teste prático para vaga de desenvolvedor Front End. O projeto é desenvolvido em JavaScript utilizando React.

## 🏁 Iniciando
Antes de começar, certifique-se de que seu computador tenha as versões do Node >= 8.10 e npm >= 5.6.

### Rodando o projeto
Após clonar o repositório, acesse o diretório do projeto via terminal e instale as dependências usando:

```
npm install
```

Após finalizar a instalação inicie o ambiente de desenvolvimento utilizando o comando:

```
npm start
```

A aplicação estará disponível em: http://localhost:3000/

## 🚀 Deploy
Para fazer deploy da aplicação utilize o script:

```
npm run build
```

## ⛏️ Ferramentas Utilizadas
- [React](https://pt-br.reactjs.org/docs/getting-started.html)
- [Redux](https://react-redux.js.org/)
- [Redux Saga](https://redux-saga.js.org/)
- [Styled Components](https://styled-components.com/)
- [Axios](https://github.com/axios/axios) - Para as requisições HTTP
- [Bootstrap](https://react-bootstrap.github.io/) - Uso de grids e alguns componentes
- [React Icons](https://react-icons.github.io/react-icons/) - Icones prontos pra uso